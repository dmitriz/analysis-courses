*Any feedback/question is welcome - [open an issue](https://gitlab.com/dmitriz/linear-algebra-courses/-/issues/new) or send me an email*

<!-- [![Gitter](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/Linear-Algebra-Courses/community?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge) -->

# Analysis courses - Discussion Forum

Discussion Board and File Repository for my Linear Algebra courses, where you can type mathematics formulas in Latex!

## Goals
- One place for easy and quick communication and corrections
- Quickly get answers for your quesitons or read answers to questions by others
- Improve quality and accuracy of the course content by suggesting corrections and updates


## Asking questions
To ask any new question, [open a new issue](https://gitlab.com/dmitriz/analysis-courses/-/issues/new) (see User Help below how).
You can write your question directly in the issue.
You can subscribe/unsubscribe to individual issues to receive email notifications.
**You can read any content without signing but you need to sign with Gitlab to comment or open new issues**.
Gitlab allows social logins with major services.

## Formatting
For advanced formatting, title/subtitle structure, itemization, images, videos, mathematics formulas and much more,
use the Markdown language https://docs.gitlab.com/ee/user/markdown.html

To get mathematics formulae, e.g. type `$a_1^2+b^2=c^2$` to get $a_1^2+b^2=c^2$, or `$\frac12$` to get $\frac12$ or `$$\frac12$$` to display:

$$\frac12,$$
see here for more details: https://docs.gitlab.com/ee/user/markdown.html#math

Use math font `\mathbb C` for the field of complex numbers $\mathbb C$


## User Help
- To **read old discussions or start new**: Click the `Issues` tab from the left navigation bar (4th icon from the top). 
You can write text directly in the issue. You can subscribe to updates for each issue. 
You will be automatically subscribed any new issue you create. You can always unsubscribe from any issue.
- To **download or upload files**: Click on the `Repository` tab from the left navigation bar (3rd icon from the top). 
Select `Files`. To upload new file, click on the pull-down menu `+` in the navigation bar above (3rd navigation row) and select `Upload file`.